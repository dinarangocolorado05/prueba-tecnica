import React from 'react';
import Formulario from './Formulario';

function App() {
  return (
    <div className="App">
      <h1>Crear empleado</h1>
      <Formulario />
    </div>
  );
}

export default App;
