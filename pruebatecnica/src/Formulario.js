import React, { useState } from 'react';

function Formulario() {
    const [formData, setFormData] = useState({

    });


  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
};


const handleSubmit = (e) => {
    e.preventDefault();

    console.log(formData);
};

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor="nombre">Nombre Completo *</label>
        <input
          type="text"
          id="nombre"
          name="nombre"
          placeholder='Nombre Completo'
          value={formData.nombre || ''}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="correo">Correo electronico *</label>
        <input
          type="text"
          id="correo"
          name="correo"
          placeholder='Correo electronico'
          value={formData.correo || ''}
          onChange={handleChange}
        />
      </div>
      <div>
        <div className="form-check">
        <label htmlFor="Sexo">Sexo *</label>
        <input className="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" />
        <label className="form-check-label" htmlFor="flexRadioDefault1">
            Masculino
        </label>
        </div>
        <div className="form-check">
        <input className="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked />
        <label className="form-check-label" htmlFor="flexRadioDefault2">
            Femenino
        </label>
    </div>
      </div>
      <div>
        <label htmlFor="area">Area *</label>
        <select className="form-select" aria-label="Default select example">
            <option defaultValue>Administracion</option>
            <option value="1">Gerente</option>
            <option value="2">Jefe</option>
            <option value="3">Supervisor</option>
        </select>
      </div>
      <div>
        <label htmlFor="Descripcion">Descripcion *</label>
        <input
          type="text"
          id="descripcion"
          name="descripcion"
          placeholder='Descripcion de la experiencia del empleado'
          value={formData.descripcion || ''}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="area">Roles *</label>
        <div className="form-check">
            <input className="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
            <label className="form-check-label" htmlFor="flexCheckDefault">
                Default checkbox
            </label>
            </div>
            <div className="form-check">
            <input className="form-check-input" type="checkbox" value="" id="flexCheckChecked" defaultChecked />
            <label className="form-check-label" htmlFor="flexCheckChecked">
                Checked checkbox
            </label>
</div>

      </div>
      <button className="button" type="submit">Guardar</button>
    </form>
  );
}

export default Formulario;
